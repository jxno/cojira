require "http/client"
require "colorize"

module Akiko
  class AkHTTP
    def initialize
      @client = HTTP::Client.new
    end

    private def valid?(path : String, method : String)
      uri = URI.new(SERVER, path)

      unless uri.valid
        raise Exception.new("invalid URI  has been passed to the request!")
      end

      case method
      when "GET"
        uri
      when "POST"
        # cause it's a POST, make sure we're sending our token too.
        # headers.append {"Authorization" = "Bearer #{@@auth_token}"}
        uri
      else
        uri
      end
    end

    def post(path : String, headers : HTTP, auth_token? : String)
      # if our parameters are ok
      uri = valid? path, headers

      # then make the request
      response = @client.post(uri, headers)

      unless response.status_code != 200
        puts response.status_code.colorize(:green).mode(:bold)
      else
        puts response.status_code.colorize(:red).mode(:bold)
        raise Exception.new(response.error)
      end

      response.body
    end

    def get(uri : URI, headers : HTTP.Headers)
      valid? uri, headers
      response = @client.get(uri, headers)

      unless response.status_code != 200
        response.body
      else
        raise Exception.new(response.error)
      end
    end
end
